#include <stdio.h>
#include "SDL.h"

#define GRAPHICS_X 640
#define GRAPHICS_Y 480

SDL_Surface *sdl_screen;
SDL_Event sdl_event;

#define NUM_PACKETS 3
char packets[NUM_PACKETS];

void clear_packets() {
	int i;
	for(i = 0; i < NUM_PACKETS; i++) {
		packets[i] = 0;
	}
}

void print_packets() {
	printf("%2X | %2X | %2X\n", packets[0], packets[1], packets[2]);
}

/*
 * Byte 1 
 *  X | 1 | LB | RB | Y7 | Y6 | X7 | X6 |
 * 
 * Byte 2 
 *  X | 0 | X5 | X4 | X3 | X2 | X1 | X0 |
 * 
 * Byte 3 
 *  X | 0 | Y5 | Y4 | Y3 | Y2 | Y1 | Y0 | 
 *  
 * 
 * Bit     Description 
 * ---------------------------------------------------------------------
 *  X      Not used 
 *  1      Always 1 
 *  0      Always 0 
 * ---------------------------------------------------------------------
 *  LB     Left button 
 *            0 = pressed 
 *            1 = not pressed
 * 
 *  RB     Right button 
 *            0 = pressed 
 *            1 = not pressed 
 * ---------------------------------------------------------------------
 *  X7-X0  8-bit X-movement indicator (number of mickeys) moved in the 
 * horizontal direction since the last data transmission.
 * 
 *  Y7-Y0  8-bit Y-movement indicator (number of mickeys) moved in the 
 * horizontal direction since the last data transmission. 
 */

void translate_motion() {
	packets[0] = 0x40;
	packets[0] |= (sdl_event.motion.state & 1) << 5; // LB
	packets[0] |= (sdl_event.motion.state & 4) << 2; // RB
	packets[0] |= ((unsigned char) ((char)sdl_event.motion.yrel) & ((char) 0xC0)) >> 4; // Y7-Y6
	packets[0] |= ((unsigned char) ((char)sdl_event.motion.xrel) & ((char) 0xC0)) >> 6; // X7-X
	
	packets[1] = ((char)sdl_event.motion.xrel) & ((char) 0x3F); // X5-X0
	packets[2] = ((char)sdl_event.motion.yrel) & ((char) 0x3F); // Y5-Y0
}

void translate_down() {
	packets[0] = 0x40;
	packets[0] |= (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)) << 5; // LB
	packets[0] |= (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT)) << 2; // RB
	
	packets[1] = 0;
	packets[2] = 0;
}

void translate_up() {
	packets[0] = 0x40;
	packets[0] |= (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)) << 5; // LB
	packets[0] |= (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT)) << 2; // RB
	
	packets[1] = 0;
	packets[2] = 0;
}

int main(char** argv, int argc) {
    int i;
    printf("%d\n", sizeof(char));

    SDL_Init(SDL_INIT_VIDEO);
	sdl_screen = SDL_SetVideoMode(GRAPHICS_X, GRAPHICS_Y, 8, 0);

    for (i = 0; i < GRAPHICS_X * GRAPHICS_Y; i++)
    	((unsigned char *)sdl_screen->pixels)[i] = 0;
    SDL_Flip(sdl_screen);

    i = 1;
	while(i) {
		if(SDL_PollEvent(&sdl_event)) {
			switch(sdl_event.type) {
				case SDL_MOUSEMOTION: 
					translate_motion();
					print_packets();
					printf("\t(%3d,%3d)", (char)sdl_event.motion.xrel, (char)sdl_event.motion.yrel);
					printf("\tl=%d, r=%d\n", !!(sdl_event.motion.state & 1), !!(sdl_event.motion.state & 4));
					clear_packets();
					break;
				case SDL_MOUSEBUTTONDOWN:
					translate_down();
					print_packets();
					clear_packets();
					break;
				case SDL_MOUSEBUTTONUP: 
					translate_up();
					print_packets();
					clear_packets();
					break;
				case SDL_QUIT:
					i = 0;
			}
		}
	}

	SDL_Quit();
    return 0;
}
