; ************************* INT 33h - mouse driver

int33:

	cmp	al, 0
	je	int33_reset_mouse
	cmp	al, 3
	je	int33_get_pos
	cmp	al, 4
	je	int33_set_pos
	cmp	al, 7
	je	int33_set_hor_pos
	cmp	al, 8
	je	int33_set_ver_pos
	cmp	al, 0x0b
	je	int33_get_mouse_movement
	cmp	al, 0x0c
	je	int33_set_subroutines
	cmp	al, 0x14
	je	int33_swap_subroutines

	iret

int33_reset_mouse:
	mov	ax, 0xffff	; there is a mouse
	mov	bx, 0xffff	; the mouse has two buttons
	iret

int33_get_pos:
	push	ax

	mov	ax, 0
	mov	dx, 0x3fc	; get mouse button state
	in	al, dx
	push	ax		; save it in the stack

	mov	ax, 0
	mov	dx, 0x3f8	; get X pos low byte
	in	al, dx
	mov	cl, al

	mov	ax, 0
	mov	dx, 0x3f9	; get X pos high byte
	in	al, dx
	mov	ch, al

	push	cx		; save it in the stack

	mov	ax, 0
	mov	dx, 0x3fa	; get Y pos low byte
	in	al, dx
	mov	cl, al

	mov	ax, 0
	mov	dx, 0x3fb	; get Y pos high byte
	in	al, dx
	mov	ch, al

	mov	dx, cx		; Y pos in DX
	pop	cx		; X pos in CX
	pop	bx		; buttons in BX

	pop	ax
	iret

int33_set_pos:
int33_set_hor_pos:
int33_set_ver_pos:
int33_get_mouse_movement:
int33_set_subroutines:
int33_swap_subroutines:
	iret
